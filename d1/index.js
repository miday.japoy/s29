//require directive tells us to load the express module
const express = require('express');

//creating a server using express
const app = express();


//port 
const port = 4000;

//middlewares
app.use(express.json())
	//allows app to read a json data
app.use(express.urlencoded({extended: true}));
	//allows app to read data from forms
	//by default, information received from thr url can only be received as string or an array
	//with extended: true, this allows to receive infor,ation in other data types such as objects.


// mock database

let users = [
	
	{
		email: "nezukoKamado@gmail.com",
		username: "nezoku01",
		password: "letMeOut",
		isAdmin: false
	},

	{
		email: "tanjiroKamado@gmail.com",
		username: "gonpachiro",
		password: "iAmTanjiro",
		isAdmin: false
	},

	{
		email: "zenitsuAgamatsuma@gmail.com",
		username: "zenitsuSleeps",
		password: "iNeedNezuko",
		isAdmin: false
	}



]

let loggedUser;

// GET METHOD

app.get('/', (req, res) => {
	res.send('Hello World')
});

app.get('/hello', (req, res) => {
	res.send('Hello from Batch 131')
});

// POST METHOD

app.post('/', (req, res) => {
	console.log(req.body);
	res.send(`Hello I am ${req.body.name}, I am ${req.body.age}. I could be described as ${req.body.description}.`)
});
// use console.log to check whats inside the request body

app.post('/users/register', (req, res) => {
	console.log(req.body);

	let newUser = {
		email: req.body.email,
		username: req.body.username,
		password: req.body.password,
		isAdmin: req.body.isAdmin
	};

	users.push(newUser)
	console.log(users);

	res.send(`User ${req.body.username} has successfully registered.`)
})

//LOGIN ROUTE

app.post('/users/login', (req, res) => {
	console.log(req.body);

	let foundUser = users.find((user) => {

		return user.username == req.body.username && user.password === req.body.password;
	});

	if(foundUser !== undefined){
		let foundUserIndex = users.findIndex((user) => {

			return user.username === foundUser.username
		});

		foundUser.index = foundUserIndex;

		loggedUser = foundUser;

		console.log(loggedUser)

		res.send(`Thank you for loggin in.`)

	} else{
		loggedUser = foundUser;

		res.send(`Login Failed, wrong credentials.`)

	}


})

/*
// Change-Password Route

app.put('/users/change-password', (req, res) => {

	let message;

	for(let i = 0; i < users.length; i++){

		if(req.body.username === users[i].username){

			user[i].password = req.body.password;

			message = `User ${req.body.username}'s password has been changed.`;

			break;	

		} else {

			message = `User not found.`
		}
	}

	res.send(message);	
})*/

// Activity Solution


app.get('/home', (req, res) => {
	res.send('Welcome Home!')
});

app.get('/users', (req, res) => {
	res.send(users)
});


//app - server
//get - HTTP method
// '/' - route name or endpoint
//(req, res) - request and response - will handle the requests and the responses
//res.send - combines writeHead() and end(), used to send response to our client


//listen to the port and returning message in the  terminal.
app.listen(port, () => console.log(`The Server is running at port ${port}`));


